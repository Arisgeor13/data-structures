public class Snake {
	
	private int snakeId;
	private int headId;
	private int TailId;
	
	public Snake(Snake theSnake) {
		
	}
	
	public Snake () {}
	
	public Snake (int tailid, int snakeid, int headid) {
		this.snakeId = snakeid;
		this.headId = headid;
		this.TailId = tailid;
	}
	
	public int getSnakeId() {
		return this.snakeId;
	}
	
	public int getheadId() {
		return this.headId;
	}
	
	public int getTailId () {
		return this.TailId;
	}
	
	public void setSnakeId(int snakeid) {
		this.snakeId = snakeid;
	}
	
	public void setheadId (int headid) {
		this.headId = headid;
	}
	
	public void setTailId (int tailid) {
		this.TailId = tailid;
	}

}
