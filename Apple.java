public class Apple {
	private int appleId;
	private int appleTileId;
	private String color;
	private int points;
	
	public Apple() {
	}
	
	public Apple (Apple myApple) {
	}
	
	public Apple (int appleid, int appletileid, String color, int points) {
		this.appleId = appleid;
		this.appleTileId = appletileid;
		this.color = color;
		this.points = points;
	}
	
	public int getappleId () {
		return this.appleId;
	}
	
	public int getappleTileId () {
		return this.appleTileId;
	}
	
	public String getcolor () {
		return this.color;
	}
	
	public int getpoints() {
		return this.points;
	}
	
	public void setappleId(int appleid) {
		this.appleId = appleid;
	}
	
	public void setappleTileId(int appletileid) {
		this.appleTileId = appletileid;
	}
	
	public void setcolor (String color) {
		this.color = color;
	}
	
	public void setpoints (int points) {
		this.points = points;
	}
}
