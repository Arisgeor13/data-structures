public class Ladder {
	private int ladderId;
	private int upStepId;
	private int downStepId;
	private boolean broken;
	
	public Ladder () {
	}
	
	public Ladder (Ladder myLadder) {
	}
	
	public Ladder (int id, int upstepid, int downstepid, boolean broken) {
		this.ladderId = id;
		this.upStepId = upstepid;
		this.downStepId = downstepid;
		this.broken = broken;
	}
	
	public int getladderId() {
		return this.ladderId;
	}
	
	public int getupStepId() {
		return this.getupStepId();
	}
	
	public int getdownStepId() {
		return this.downStepId;
	}
	
	public boolean getbroken () {
		return this.broken;
	}
	
	public void setladderId(int id) {
		this.ladderId = id;
	}
	
	public void setupStepId(int upstepid) {
		this.upStepId = upstepid;
	}
	
	public void setdownStepId (int downstepid) {
		this.downStepId = downstepid;
	}
	
	public void setbroken(boolean broken) {
		this.broken = broken;
	}
	
}
